require "rubygems"
require "rofl"
require "Qt4"
require "graph"
#require "unprof"

#oky, this will become a visualizer for large data sequences

class Gui < Qt::Widget
  
  attr_accessor :width,:height
  attr_accessor :x_delta,:mousex,:mousey
  
  def initialize(width,height,parent = nil)
    super()
    @width,@height = width,height
    resize(@width,@height)
    @lfw = 18 # legend font width
    @tfw = 16 # timestamp font width
    @pfw = 11 # predicate font width
    #text will not be filled
    #@brush = Qt::NoBrush
    #text will be filled
    @brush = Qt::Brush.new Qt::Color.new 0, 0, 0
    @current_color = 0
    #generate these deltas
    deltas = [:simple,:pos,:rot]
    #display these predicates
    #@predicates = [:boh, :pd, :wp, :we]
    @predicates = []
    #graph with the data we want to visualize
    @graph = Graph.new "V06",deltas,@height/12
    #@graph = Graph.new "",deltas,@height/12,true #will build graph from table 'TrackerTest'
    #max movement speed
    @speedlimit = 20
    #setup the rest
    setup
    #method doing the x scrolling for us
    dlog "Initialized, starting active scroll loop."
    reactive_scroll
  end
  
  #setup all the stuff we need to display
  def setup
    #legend painter path
    @legend = build_legend_path
    #text painter path
    @text = build_text_path
    #graph painter paths
    @graphs = setup_graph_paths
    #now some gui specific stuff
    setPalette(Qt::Palette.new(Qt::Color.new(250, 250, 250)))
    setAutoFillBackground(true)
    #cursor settings
    @cur1 = self.cursor
    @cur2 = Qt::Cursor.new(Qt::PointingHandCursor)
    self.mouseTracking = true
    #used for scrolling objects to the left and right
    @xdelta,@xgoal,@xspeed = 0.0,0.0,0.0
  end
  
  #build painter path for the legend
  def build_legend_path
    p = Qt::PainterPath.new
    p.moveTo 0,0
    font = Qt::Font.new "Bitstream Vera Sans", @lfw
    p.addText @lfw,1.5*@lfw, font, "LEFT: euler angles"
    p.addText @lfw,2*@height/12, font, "RIGHT: euler angles"
    p.addText @lfw,5*@height/12, font, "LEFT: data"
    p.addText @lfw,7*@height/12, font, "RIGHT: data"
    p.addText @lfw,10*@height/12-@lfw, font, "LEFT: blackout"
    p.addText @lfw,10*@height/12+@lfw, font, "RIGHT: blackout"
    p.addText @lfw,11*@height/12-@lfw/2, font, "timestamps"
    dlog "Finished legend path setup."
    return p
  end
  
  #build painter path for the text data
  def build_text_path
    #y to timestamps
    ty = 11*@height/12
    #back to left hand data
    ypl = -5*@height/12-2*@lfw
    #and back to right hand data
    ypr = -3*@height/12-2*@lfw
    tfont = Qt::Font.new "Bitstream Vera Sans", @tfw
    pfont = Qt::Font.new "Bitstream Vera Sans", @pfw
    every = 25 #display text every ... frames
    current = 0
    p = Qt::PainterPath.new
    p.moveTo 0,0
    @graph.points.each do |ts,hands|
      if current >= every
        p.addText hands[:left][:x],@lfw, tfont, "#{ts}"
        #display the predicates
        @predicates.each_index do |i|
          p.addText hands[:left][:x], ypl+i*@lfw, pfont, "#{hands[:left][@predicates[i]]}" unless hands[:left][:blackout]
          p.addText hands[:right][:x], ypr+i*@lfw, pfont, "#{hands[:right][@predicates[i]]}" unless hands[:right][:blackout]
        end
        current = 0
      else
        current += 1
      end
    end
    dlog "Finished subtitles path setup."
    return { :y => ty, :path => p }
  end
  
  #setup the graph paths we want to paint
  def setup_graph_paths
    dlog "Starting graph path setup."
    graph_paths = {}
    #build following graphs for both hands
    [:left,:right].each do |hand| 
      #build painter paths for blackout graph
      blackout = build_blackout_path(hand)
      graph_paths[blackout[:name]] = blackout
      dlog "Finished blackout path setup. Y: #{blackout[:y]}"
      #build painter paths for euler angles
      [:alpha,:beta,:gamma].each do |angle|
        euler = build_euler_path(hand,angle)
        graph_paths[euler[:name]] = euler
        dlog "Finished #{euler[:name]} path setup. Y: #{euler[:y]}"
      end
      #build painter paths for deltas
      @graph.deltas.each do |d|
        delta = build_delta_path(hand,d)
        graph_paths[delta[:name]] = delta
        dlog "Finished #{delta[:name]} path setup. Y: #{delta[:y]}"
      end        
    end    
    dlog "Finished graph path setup."
    return graph_paths
  end
  
  #build painter path for blackout graph
  def build_blackout_path hand
    name = "blackout_#{hand}".to_sym
    color = Qt::Color.new(0, 0, 64) if hand.eql? :left
    color = Qt::Color.new(64, 0, 0) if hand.eql? :right
    #y-translation of the graph, we only need y
    ty = 10*@height/12+@lfw/2
    ty -= @lfw if hand.eql? :left
    ty += @lfw if hand.eql? :right
    path = Qt::PainterPath.new
    path.moveTo 0,0
    #now we construct the path
    @graph.points.each do |ts,v|
      unless v[hand][:blackout]        
       path.moveTo v[hand][:x],0
      else
       path.lineTo v[hand][:x],0
      end   
    end
    #and return the values
    return { :y => ty, :name => name, :path => path, :color => color, :width => 2.5 }
  end

  #build painter path for euler graph
  def build_euler_path hand,angle  
    name = "#{angle}_#{hand}".to_sym
    color = Qt::Color.new(255, 0, 0) if angle.eql? :alpha
    color = Qt::Color.new(0, 255, 0) if angle.eql? :gamma
    color = Qt::Color.new(0, 0, 255) if angle.eql? :beta
    #y-translation of the graph, we only need y
    ty = @height/6
    path = Qt::PainterPath.new
    path.moveTo 0,0
    #now we construct the path
    @graph.points.each do |ts,v|
      path.lineTo v[hand][:euler][angle][:point]
    end
    #and return the values
    return { :y => ty, :name => name, :path => path, :color => color }
  end
  
  #build painter path for delta graph
  def build_delta_path hand,delta   
    name = "#{delta}_#{hand}".to_sym
    color = Qt::Color.new(64, 0, @current_color%255) if hand.eql? :left
    color = Qt::Color.new(@current_color%255, 0, 64) if hand.eql? :right
    #change color for next delta
    @current_color += 16
    ty = 7*@height/12
    path = Qt::PainterPath.new
    path.moveTo 0,0
    #now we construct the path
    @graph.points.each do |ts,v|
      path.lineTo v[hand][:deltas][delta][:point]
    end
    #and return the values
    return { :y => ty, :name => name, :path => path, :color => color }
  end
  
  #gets called when a repaint is necessary
  def paintEvent(event)
    paint_graphs
    paint_text
    paint_legend
  end
  
  #outsourced graph painting
  def paint_graphs
    p = Qt::Painter.new(self)
    p.setRenderHint(Qt::Painter::HighQualityAntialiasing)
    p.setBrush Qt::NoBrush
    @graphs.each do |name,path|
      p.resetMatrix
      p.translate((@width/2)-@xdelta,path[:y])
      pen = Qt::Pen.new(Qt::SolidLine)
      pen.setColor path[:color]
      if path.has_key? :width
        pen.setWidth path[:width]
      else
        pen.setWidth 1.5
      end
      p.setPen(pen)
      p.drawPath path[:path]
    end
    p.end()
  end

  #outsourced text painting
  def paint_text
    p = Qt::Painter.new(self)
    p.setRenderHint(Qt::Painter::HighQualityAntialiasing)
    p.setBrush Qt::NoBrush
    p.resetMatrix
    p.translate((@width/2)-@xdelta,@text[:y])
    pen = Qt::Pen.new(Qt::SolidLine)
    pen.setWidth 2
    p.setPen(pen)
    p.drawPath @text[:path]
    p.end()
  end
  
  #outsourced legend painting
  def paint_legend
    p = Qt::Painter.new(self)
    p.setRenderHint(Qt::Painter::Antialiasing)
    p.resetMatrix
    p.translate(0,0)
    p.setBrush @brush
    pen = Qt::Pen.new(Qt::SolidLine)
    pen.setWidth 1
    p.setPen(pen)
    p.drawPath @legend
    p.end()
  end

  #mouse press event
  def mousePressEvent event
    if event.buttons == Qt::RightButton
    elsif event.buttons == Qt::LeftButton
      #update
    end
  end
  
  #a mouse move event
  def mouseMoveEvent event
    @mousex,@mousey = event.x,event.y
  end
  
  #ruby thread test
  def reactive_scroll
    fps = 60.0 #frames per second we attempt to compute
    tts = 0.0 #time to sleep
    last_time = Time.now
    Thread.new do
      loop do 
        tts = 1.0/fps - (Time.now-last_time) 
        sleep tts if tts > 0
        unless @mousex.nil?
          scroll_x 
          smooth_delta
          if @xspeed.abs > 1 
            #are we speeding?
            if @xspeed < -1.0*@speedlimit
              @xspeed = -1.0*@speedlimit
              @xgoal = @xdelta
            end
            if @xspeed > @speedlimit
              @xspeed = @speedlimit 
              @xgoal = @xdelta
            end
            #new delta
            @xdelta = (@xdelta + @xspeed).to_i
            update
          end
        end
        last_time = Time.now
      end
    end
  end
  
  #scroll on the x_axis if necessary
  def scroll_x
    damper = 10
    #need some kind of zone that triggers scrolling: 1/4 width left and right
    if @mousex < @width/4
      @xgoal += (@mousex - @width/4)/damper  
    end
    if @mousex > 3*@width/4
      @xgoal += (@mousex - 3*@width/4)/damper
    end
  end
  
  #method that smoothes the scroll deltas over time
  def smooth_delta
    @xspeed = (@xgoal-@xdelta)*0.05
  end
end

#start the qt application
app = Qt::Application.new(ARGV)
gui = Gui.new(800,700)
#dirty qt timer magic to make ruby threads work
block=Proc.new{ Thread.pass }
timer=Qt::Timer.new(gui)
invoke=Qt::BlockInvocation.new(timer, block, "invoke()")
Qt::Object.connect(timer, SIGNAL("timeout()"), invoke, SLOT("invoke()"))
timer.start(10) #in millis
#end of dirty timer hack
gui.show()
app.exec()
