require "rubygems"
require "rofl"

class Calibration
	attr_accessor :xfat,:yfat,:zfat,:body,:neck_fix
	attr_accessor :offset
	
	def initialize
		# default values for the calibration
		@yfat,@xfat,@zfat = 0,0,0 # mcneil grid correction in cm
    @body = 20 #thickness of the body in cm
    @neck_fix = -20 # neck sensor correction in degree 
	end
	
	#trial specific calibration values for V06
	def trial06
		@yfat = 0 #cm
    @xfat = 0 #cm
    @zfat = 0 #cm
    @body = 20 #thickness of the body
    @neck_fix = -20 #degree
    @offset = 500 # in milliseconds 		
	end
	
	#trial specific calibration values for V07
	def trial07
		@yfat = 0 #cm
    @xfat = 0 #cm
    @zfat = 0 #cm
    @body = 20 #thickness of the body
    @neck_fix = -20 #degree 
    @offset = 1000 # in milliseconds
	end

	
	# set trial specific calibration data
	def set_trial trial
		trial01 if trial.include? "01"
		trial02 if trial.include? "02"
		trial03 if trial.include? "03"
		trial04 if trial.include? "04"
		trial05 if trial.include? "05"
		trial06 if trial.include? "06"
		trial07 if trial.include? "07"
		trial08 if trial.include? "08"
		trial09 if trial.include? "09"
		trial10 if trial.include? "10"
		trial11 if trial.include? "11"
		trial12 if trial.include? "12"
		trial13 if trial.include? "13"
		trial14 if trial.include? "14"
		trial15 if trial.include? "15"
		trial16 if trial.include? "16"
		trial17 if trial.include? "17"
		trial18 if trial.include? "18"
		trial19 if trial.include? "19"
		trial20 if trial.include? "20"
		trial21 if trial.include? "21"
		trial22 if trial.include? "22"
		trial23 if trial.include? "23"
		trial24 if trial.include? "24"
		trial25 if trial.include? "25"	
	end
	
	# catch calls we haven't implemented yet
	def method_missing(method_name, *args) 
	  puts "Tried to call #{method_name} with args: #{args * ', '}"
	  puts "Need to implement it! :)"
  end  
end

=begin
TRIAL SPECIFIC INFORMATION

v1 -9500ms

v2 -47500ms

v3 neckdata insufficient

v4 -6000ms
Parsed neckdata count is 45060
Parsed lefthanddata count is 33149
Parsed righthanddata count is 150
Left hand quality is 73.566355969818%
Right hand quality is 0.33288948069241%

v5 -10400ms
Parsed neckdata count is 84624
Parsed lefthanddata count is 76783
Parsed righthanddata count is 30185
Left hand quality is 90.7343070523729%
Right hand quality is 35.6695500094536%

v6 500ms
Parsed neckdata count is 42030
Parsed lefthanddata count is 40707
Parsed righthanddata count is 10973
Left hand quality is 96.8522483940043%
Right hand quality is 26.1075422317392%

v7 1000ms LEFT HAND REVERSED
Parsed neckdata count is 31169
Parsed lefthanddata count is 31047
Parsed righthanddata count is 678
Left hand quality is 99.6085854534955%
Right hand quality is 2.17523821745966% 

v8 insufficient data 

v9 neck sensor covered?

v10 neck sensor covered?

v11 insufficient data for offset guessing
Parsed neckdata count is 67455
Parsed lefthanddata count is 15223
Parsed righthanddata count is 7585
Left hand quality is 22.5676376843822%
Right hand quality is 11.2445333926321%

v12 500ms
Parsed neckdata count is 16006
Parsed lefthanddata count is 14576
Parsed righthanddata count is 722
Left hand quality is 91.0658503061352%
Right hand quality is 4.51080844683244%

v13 insufficient data for offset guessing
Parsed neckdata count is 44563
Parsed lefthanddata count is 5782
Parsed righthanddata count is 0
Left hand quality is 12.9748894823059%
Right hand quality is 0.0%

v14 necksensor partially covered?
Parsed neckdata count is 2936
Parsed lefthanddata count is 13213
Parsed righthanddata count is 470
Left hand quality is 450.034059945504%
Right hand quality is 16.008174386921%

v15 1250ms
Parsed neckdata count is 53679
Parsed lefthanddata count is 40131
Parsed righthanddata count is 2346
Left hand quality is 74.7610797518583%
Right hand quality is 4.37042418823003%

v16 -500ms
Parsed neckdata count is 18928
Parsed lefthanddata count is 15745
Parsed righthanddata count is 1119
Left hand quality is 83.1836432797971%
Right hand quality is 5.91187658495351%

v17 -2000ms
Parsed neckdata count is 58815
Parsed lefthanddata count is 56093
Parsed righthanddata count is 981
Left hand quality is 95.3719289296948%
Right hand quality is 1.66794185156848%

v18 necksensor covered?
Parsed neckdata count is 274
Parsed lefthanddata count is 35133
Parsed righthanddata count is 10321
Left hand quality is 12822.2627737226%
Right hand quality is 3766.78832116788%

v19 1750ms
Parsed neckdata count is 24706
Parsed lefthanddata count is 22372
Parsed righthanddata count is 1409
Left hand quality is 90.5529021290375%
Right hand quality is 5.70306808062819%

v20 1900ms
Parsed neckdata count is 49420
Parsed lefthanddata count is 47438
Parsed righthanddata count is 215
Left hand quality is 95.9894779441522%
Right hand quality is 0.435046539862404%

v21 4500ms 
Parsed neckdata count is 23294
Parsed lefthanddata count is 20892
Parsed righthanddata count is 1
Left hand quality is 89.6883317592513%
Right hand quality is 0.00429295097449987%

v22 2000ms
Parsed neckdata count is 35227
Parsed lefthanddata count is 33319
Parsed righthanddata count is 868
Left hand quality is 94.5837000028387%
Right hand quality is 2.46401907627672%

v23 insufficient data

v24 -12000ms
Parsed neckdata count is 42627
Parsed lefthanddata count is 32389
Parsed righthanddata count is 767
Left hand quality is 75.98235859901%
Right hand quality is 1.79932906373894%

v25 7200ms
Parsed neckdata count is 21482
Parsed lefthanddata count is 21401
Parsed righthanddata count is 2403
Left hand quality is 99.6229401359277%
Right hand quality is 11.18610930081%
 
=end
