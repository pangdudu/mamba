require 'rubygems'
require 'opengl'
require 'rational'
require 'narray'
require 'rofl'
#include Gl,Glu,Glut

class OpenGlViewer

  attr_accessor :currenttimestamp,:timestamps,:pause

  STDOUT.sync = TRUE
  POS = [0, 0, 0, 0.0]

  def initialize data
    @data = data
    @base = NMatrix.float(4,4).unit
    @matrices = {}
    @matrices[:base] = @base
    @currenttimestamp = 0.0
    @current = 0
    @timestamps = {}
    @pause = true
    @forward = true
    @leftboh = "none"
    @rightboh = "none"
    filltimestamps
    glShadeModel(GL::SMOOTH)
    glNormal(0.0, 0.0, 1.0)
    glLightfv(GL::LIGHT0, GL::POSITION, POS)
    glEnable(GL::CULL_FACE)
    glEnable(GL::LIGHTING)
    glEnable(GL::LIGHT0)
    glEnable(GL::DEPTH_TEST)
    glEnable(GL::LINE_SMOOTH)
    glEnable(GL::POINT_SMOOTH)
    glClearColor(0.0, 0.0, 0.0, 0.0)
  end

  def filltimestamps
    i = 0
    @data[:reference].each do |timestamp,value|
      @timestamps[i] = timestamp
      i += 1
    end
    ilog "Found '#{@timestamps.count}'"
    ilog "Will now try to display '#{@data[:left].count}' left and '#{@data[:right].count}' right hand values"
  end

  def fillmatrices
    unless @data[:left][@currenttimestamp].nil?
      @matrices[:left_reference] = @data[:left][@currenttimestamp][:reference]
      @matrices[:left_hand_relative] = @data[:left][@currenttimestamp][:relative]
      @leftboh = @data[:left][@currenttimestamp][:boh]
    end
    unless @data[:right][@currenttimestamp].nil?
      @matrices[:right_reference] = @data[:right][@currenttimestamp][:reference] 	
      @matrices[:right_hand_relative] = @data[:right][@currenttimestamp][:relative]
      @rightboh = @data[:right][@currenttimestamp][:boh]
    end
  end

  def setcurrenttimestamp
    unless @pause
      if @forward
        @current += 1
        @currenttimestamp = @timestamps[@current]
      end
      if !@forward
        @current -= 1 unless (@current.eql? 0)
        @currenttimestamp = @timestamps[@current]
      end
    end
  end

  # gl display method
  def display
    fillmatrices
    setcurrenttimestamp
    fps_control(60)
    glClear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT)
    glColor(0.0, 0.0, 0.0)
    glPushMatrix()
    glRotate(@view_rotx, 1.0, 0.0, 0.0)
    glRotate(@view_roty, 0.0, 1.0, 0.0)
    glRotate(@view_rotz, 0.0, 0.0, 1.0)
    @matrices.each { |k,v| draw_object k,v }
    displayinfo
    glPopMatrix()
    glutSwapBuffers()
    @frames = 0 if not defined? @frames
    @t0 = 0 if not defined? @t0
    @frames += 1
    t = GLUT.Get(GLUT::ELAPSED_TIME)
    if t - @t0 >= 5000
      seconds = (t - @t0) / 1000.0
      fps = @frames / seconds
      @t0, @frames = t, 0
      exit if defined? @autoexit and t >= 999.0 * @autoexit
    end
  end

  def drawOneLine x1,y1,z1,x2,y2,z2
    glBegin(GL_LINES)
    glVertex(x1,y1,z1)
    glVertex(x2,y2,z2)
    glEnd()
  end

  def drawLineWithText text,scale,x,y,z
    scale2 = scale*scale
    glPushMatrix()
    drawOneLine(0,0,0,scale*x,scale*y,scale*z)
    glTranslate((scale*x),(scale*y),(scale*z))
    glScale(1.0/(scale2),1.0/(scale2),1.0/(scale2))
    text.each_byte { |x| glutStrokeCharacter(GLUT_STROKE_ROMAN, x) }
    glScale(scale2,scale2,scale2)
    glTranslate(-(scale*x),-(scale*y),-(scale*z))
    glPopMatrix()
  end

  # draw an imd and its childs
  def draw_object key,matrix
    glPushMatrix()
    scale = 10
    translation_scale = 2
    glTranslate(matrix[3,0]*scale*translation_scale,matrix[3,1]*scale*translation_scale,matrix[3,2]*scale*translation_scale)
    glColor(1,0,0)
    drawLineWithText "#{key} x",scale,matrix[0,0],matrix[0,1],matrix[0,2]
    glColor(0,1,0)
    drawLineWithText "#{key} y",scale,matrix[1,0],matrix[1,1],matrix[1,2]
    glColor(0,0,1)
    drawLineWithText "#{key} z",scale,matrix[2,0],matrix[2,1],matrix[2,2]
    glTranslate(-matrix[3,0]*scale*translation_scale,-matrix[3,1]*scale*translation_scale,-matrix[3,2]*scale*translation_scale)
    glPopMatrix()
  end

  def displayinfo
    info = "Information:"
    info += " timestamp: #{(@timestamps[@current]).to_s}"
    info += " PAUSED" if @pause
    info += " FORWARD" if (!@pause && @forward)
    info += " REVERSE" if (!@pause && !@forward)
    info += " BOH left:#{@leftboh} right:#{@rightboh}"
    scale = 10
    scale2 = scale*scale
    glPushMatrix()
    glTranslate((scale),(scale),(scale))
    glScale(1.0/(scale2),1.0/(scale2),1.0/(scale2))
    info.each_byte { |x| glutStrokeCharacter(GLUT_STROKE_ROMAN, x) }
    glScale(scale2,scale2,scale2)
    glTranslate(-(scale),-(scale),-(scale))
    glPopMatrix()
  end

  def fps_control(fps)
    t = GLUT.Get(GLUT::ELAPSED_TIME)
    @t0_idle = t if !defined? @t0_idle
    time_to_sleep = (1000./fps) - (t - @t0_idle)
    sleep(time_to_sleep.to_f/1000) if time_to_sleep > 0
    @t0_idle = t
  end

  # gets called when window is resized etc.
  def reshape width, height
    h = height.to_f / width.to_f
    glViewport(0, 0, width, height)
    glMatrixMode(GL::PROJECTION)
    glLoadIdentity()
    glFrustum(-1.0, 1.0, -h, h, 1.0, 1000.0)
    glMatrixMode(GL::MODELVIEW)
    setViewpoint
  end

  def idle
    GLUT.PostRedisplay()
  end

  def setViewpoint
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(@eyex, 0, @znear, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)
    glutPostRedisplay()
  end

  # Change view angle
  def special (k, x, y)
    case k
    when GLUT::KEY_UP
      @view_rotx += 5.0
    when GLUT::KEY_DOWN
      @view_rotx -= 5.0
    when GLUT::KEY_LEFT
      @view_roty += 5.0
    when GLUT::KEY_RIGHT
      @view_roty -= 5.0
    end
    GLUT.PostRedisplay()
  end

  # mouse ;)
  def mouse button, state, x, y
    changed = false
    case button
    when 3 then @eyex -= 5; changed = true;
    when 4 then @eyex += 5; changed = true
    end
    setViewpoint if changed
    @mouse = state
    @x0, @y0 = x, y
  end

  # motion control
  def motion x, y
    if @mouse == GLUT::DOWN then
      @view_rotz += @x0 - x
      @view_roty += @y0 - y
      setViewpoint
    end
    @x0, @y0 = x, y
  end

  #keyboard control handling
  def keyboard(key, x, y)
    case (key)
    when ?s
      @znear = @znear - 1
    when ?w
      @znear = @znear + 1
    when ?a
      @eyex = @eyex - 1
    when ?d
      @eyex = @eyex + 1
    when ?f
      @forward = true
    when ?r
      @forward = false
    when ?p
      togglepause
    when 27 # Escape
      exit
    end
    setViewpoint
  end

  def togglepause
    @pause = !@pause
    puts "Toggle pause now '#{@pause}'"
  end

  def visible(vis)
    GLUT.IdleFunc((vis == GLUT::VISIBLE ? method(:idle).to_proc : nil))
  end
  #the loop
  def run
    @znear = 30
    @eyex = 0
    @view_rotx, @view_roty, @view_rotz = 0, 0, 0
    glutInit
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB)
    glutInitWindowSize(640, 480)
    glutInitWindowPosition(0, 0)
    glutCreateWindow($0)
    glutDisplayFunc(method(:display).to_proc)
    glutReshapeFunc(method(:reshape).to_proc)
    glutSpecialFunc(method(:special).to_proc)
    glutKeyboardFunc(method(:keyboard).to_proc)
    glutMouseFunc(method(:mouse).to_proc)
    glutMotionFunc(method(:motion).to_proc)
    glutVisibilityFunc(method(:visible).to_proc)
    glShadeModel(GL::SMOOTH)
    glNormal(0.0, 0.0, 1.0)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glutMainLoop()
  end
end
