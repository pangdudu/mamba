=begin
  This file e.g. the TrackerDataParser fetches the tracker data from the 
  database, and parses them according to the "SFB673TPB1 Annotation Manual".
=end

require "rubygems"
require "narray"
require "sequel"
require "rofl"
require "calibration"

#connect to the database
DB = Sequel.connect(:adapter=>'mysql', :host=>'localhost', :database=>'saga', :user=>'saga', :password=>'mysql!secret')

class TrackerDataParser
  
  attr_accessor :data
  
  def initialize trial,test=false
  	@trial = trial
  	cal = Calibration.new # you should specify trial specific calibrations in here
    cal.set_trial trial
    @yfat,@xfat,@zfat = cal.yfat,cal.xfat,cal.zfat
    @body = cal.body
    @neck_fix = cal.neck_fix 
    tablename = "Tracker#{trial}"
    tablename = "TrackerTest" if test
    dlog "Trying to connect to table: #{tablename}"
    #get our rows
    @rows = DB[tablename.to_sym]
    @highpass = 0.4 #for later filtering
    dlog "Table has #{@rows.count} rows."
    #processed data will end up here
    @data = {:left => {}, :right => {}}
    #do we know that one marker has been worn wrong?
    @data[:left_twist] = false
    @data[:right_twist] = false
    #neck rotation compensation angle
    @na = Math::PI/4
    #neck rotation compensation matrix (rot around x)
    @nr = NMatrix.float(4,4).unit
    @nr[1,1], @nr[1,2], @nr[2,1], @nr[2,2] = Math.cos(@na),Math.sin(@na),-Math.sin(@na),Math.cos(@na)
    #get and parse the tracker data
    operate
  end
  
  # THIS IS SPECIAL METHOD FOR THE OD to generate a csv file
  def rock_n_roll_csv_apfelzauber filename
    # nerviger typ switch
    nerv = true
    [:left,:right].each do |hand| 
      csv = File.new("#{filename}_#{hand.to_s}.csv"_,"w")
      csv.sync = true # now data is written directly to the file
      # change the header if your strong enough
      unless nerv
        predicates = [:boh,:pd,:wp,:we]
        csv_header = "\"Timestamp\""
        predicates.each { |h| csv_header += ",\"#{h.to_s}\""}
        #also header waere jetzt "Timestamp","boh","pd","wp","we" kannst die reihenfolge aber auch in dem array aendern
        csv.puts csv_header
        #jetzt gehts an die daten
        @data[hand].sort.each do |timestamp,data|
          line = "\"#{timestamp}\""
          predicates.each do |p| 
            pd = data[p] #sollte jetzt :pd,:wp ... aus den daten holen
            line += ",\"#{pd}\""
          end
          #und ins file schreiben
          csv.puts line
        end
      else
        cells = {}
        (0..3).each do |i|
          (0..3).each do |j|
            cells["i:#{i},j:#{j}"] = {:i => i, :j => j}
          end
        end
        csv_header = "\"Timestamp\""
        cells.each { |name,ij| csv_header += ",\"#{name}\""}
        csv.puts csv_header
        #jetzt gehts an die daten
        @data[hand].sort.each do |timestamp,data|
          line = "\"#{timestamp}\""
          cells.each do |name,ij| 
            # nehme jetzt die korrigierte matrix oder was will der? :relative is korrigiert, :data waere roh
            matrix = data[:relative]
            i = ij[:i]
            j = ij[:j]
            cell_content = matrix[i,j]
            line += ",\"#{cell_content}\""
          end
          #und ins file schreiben
          csv.puts line
        end
      end      
    end
  end
  
  #main controller function
  def operate
    @data[:reference] = (parsetrackerdata @rows.filter(:identifier => "Neck",:timestamp => 0..60)).sort
    dlog "Parsed neckdata count is #{@data[:reference].length}"
    left_raw = (parsetrackerdata @rows.filter(:identifier => "Left_Hand",:timestamp => 0..60)).sort
    dlog "Left hand quality is #{(Float(left_raw.length)/Float(@data[:reference].length))*100}%"
    right_raw = (parsetrackerdata @rows.filter(:identifier => "Right_Hand",:timestamp => 0..60)).sort
    dlog "Right hand quality is #{(Float(right_raw.length)/Float(@data[:reference].length))*100}%"
    #sometimes people are wearing the tracker wrong, need to fix this
    fix_marker_twist
    #now parse the tracker data for both hands    
    left_raw.each do |timestamp,matrix|
      d = getannotationdata timestamp,matrix,@data[:left_polarity]
      @data[:left][timestamp] = d unless d.nil?
    end
    right_raw.each do |timestamp,matrix|
      d = getannotationdata timestamp,matrix,@data[:right_polarity]
      @data[:right][timestamp] = d unless d.nil?
    end
    #
    # CSV generation starts making sense here, because we need to run getannotationdata
    # in order to have :boh,:wp,:we
    #
    # csv annotation data will be written to:
    rock_n_roll_csv_apfelzauber "ichwarjungundbrauchtedasgeld"
    #Debug and info
    ilog "Statistics for trial #{@trial}:"
    ilog "  Left hand  - parsed: #{@data[:left].length}"
    ilog "  Right hand - parsed: #{@data[:right].length}"
  end
  
  #method parsing tracking data from the db and loading it into an hash(organized by timestamp)
  def parsetrackerdata trackerdata
    parseddata = {}
    trackerdata.each do |d|
      m = NMatrix.float(4,4)
      #that's how the coordinates would look if the matrix was transposed (now the bottom is 0,0,0,1)
      m[0,0],m[0,1],m[0,2],m[0,3] = d[:m00], d[:m01], d[:m02], d[:m03]
      m[1,0],m[1,1],m[1,2],m[1,3] = d[:m10], d[:m11], d[:m12], d[:m13]
      m[2,0],m[2,1],m[2,2],m[2,3] = d[:m20], d[:m21], d[:m22], d[:m23]
      m[3,0],m[3,1],m[3,2],m[3,3] = d[:m30], d[:m31], d[:m32], d[:m33]
      m = m.transpose #matrix data in the db are transposed
      parseddata[Float(d[:timestamp])] = m
    end
    return parseddata
  end

  def getannotationdata timestamp,matrix,polarity
    annotationdata = {}
    reference = @data[:reference][timestamp][1]
    neck = NMatrix.float(4,4).unit
    na = Math::PI/180*@neck_fix
    neck[1,1], neck[1,2], neck[2,1], neck[2,2] = Math.cos(na),-Math.sin(na),Math.sin(na),Math.cos(na)
    relative = reference.inverse * neck * matrix * polarity
    annotationdata[:timestamp] = timestamp
    annotationdata[:reference] = reference
    annotationdata[:data] = matrix
    annotationdata[:relative] = relative
    annotationdata[:boh] = getbackofhanddirection relative
    annotationdata[:pd] = getpalmdirection relative
    annotationdata[:wp] = getwristposition relative
    annotationdata[:we] = getwristextend relative
    return annotationdata
  end

  #function computing palm direction
  def getpalmdirection data
    #negative z seems to be the palm direction
    #you should check the data in the visualization to be sure
    x,y,z = (-1)*data[2,0],(-1)*data[2,1],(-1)*data[2,2]
    result = []
    result << "PAB" if z < -@highpass
    result << "PDN" if y < -@highpass
    result << "PTB" if z > @highpass
    result << "PTL" if x < -@highpass
    result << "PTR" if x > @highpass
    result << "PUP" if y > @highpass
    return result.join("/")
  end

  #function computing back of hand direction
  def getbackofhanddirection data
    #ok, it looks like the markers y axis is the boh direction
    #you should check the data in the visualization to be sure
    x,y,z = data[1,0],data[1,1],data[1,2]
    result = []
    result << "BAB" if z < -@highpass
    result << "BDN" if y < -@highpass
    result << "BTB" if z > @highpass
    result << "BTL" if x < -@highpass
    result << "BTR" if x > @highpass
    result << "BUP" if y > @highpass
    return result.join("/")
  end

  #function computing wrist extend
  def getwristextend data
    result = "aight!"
    x,y,z = data[3,0],data[3,1],data[3,2]
    #puts "wp: x '#{x}', y '#{y}', z '#{z}'"
    xf = 100 + @xfat #normalizing factor (100 = SH standard human)
    zf = 100 + @zfat #normalizing factor (100 = SH standard human)
    x = x*xf
    z = z*zf
    r = Math.sqrt(x*x + z*z)
    r -= 30 #neck sensor is on the back, so we need to substract the body
    #ok, need to put real values in here (look at the annotationmanual)
    result = "D-GTO" if r > 80+@body #greater than length of outstretched arm in front away
    result = "D-KO" if r <= 80+@body #between knee and length of outstretched arm in front away
    result = "D-EK" if r < 65+@body #between elbow and knee
    result = "D-CE" if r < 45+@body #between body and elbows length away
    result = "D-C" if r < 20+@body #in contact with body
    #puts "extend is '#{result}'"
    return result
  end

  #function computing wrist position
  def getwristposition data
    result = "aight!"
    #negative z seems to be palm direction
    #you should check the data in the visualization to be sure
    x,y,z = data[3,0],data[3,1],data[3,2]
    yf = 100 + @yfat
    xf = 100 + @xfat
    x = x*xf
    y = y*yf
    #puts "wp: x '#{x}', y '#{y}', z '#{z}'"
    #will not be rock solid, so we go from outwards->inwards
    #extreme periphery
    eperiphery = "EP-"
    eperiphery += "UP" if(y > 30 && x.abs <= 19)
    eperiphery += "UL" if(y > 17 && x < -19)
    eperiphery += "UR" if(y > 17 && x > 19)
    eperiphery += "LT" if(y <= 17 && y > -17 && x < -31.5)
    eperiphery += "RT" if(y <= 17 && y > -17 && x > 31.5)
    eperiphery += "LW" if(y < -30 && x.abs <= 19)
    eperiphery += "LL" if(y < -17 && x < -19)
    eperiphery += "LR" if(y < -17 && x > 19)
    result = eperiphery unless eperiphery.eql?("EP-")
    #periphery
    if (x.abs <= 31.5 && y.abs <= 30)
      periphery = "P-"
      periphery += "UP" if(y > 17 && x.abs <= 13)
      periphery += "UL" if(y > 9 && x < -13)
      periphery += "UR" if(y > 9 && x > 13)
      periphery += "LT" if(y <= 9 && y > -10 && x < -21)
      periphery += "RT" if(y <= 9 && y > -10 && x > 21)
      periphery += "LW" if(y <= -10 && y > -30 && x.abs <= 13)
      periphery += "LL" if(y <= -10 && y > -30 && x < -13)
      periphery += "LR" if(y <= -10 && y > -30 && x > 13)
      result = periphery unless periphery.eql?("P-")
    end
    #center
    if (x.abs <= 21 && y.abs <= 17)
      center = "C-"
      center += "UP" if(y > 9 && x.abs <= 10)
      center += "UL" if(y > 4.5 && x < -10)
      center += "UR" if(y > 4.5 && x > 10)
      center += "LT" if(y <= 4.5 && y > -7 && x < -13)
      center += "RT" if(y <= 4.5 && y > -7 && x > 13)
      center += "LW" if(y <= -7 && y > -17 && x.abs <= 10)
      center += "LL" if(y <= -7 && y > -17 && x < -10)
      center += "LR" if(y <= -7 && y > -17 && x > 10)
      result = center unless center.eql?("C-")
    end
    #center-center
    if (x.abs <= 13 && y.abs <= 9)
      result = "CC"
    end
    #puts "result #{result}"
    return result
  end
  
  #method to fix the direction of the marker, if it has been worn wrong
  def fix_marker_twist
    #left hand
    @data[:left_polarity] = NMatrix.float(4,4).unit
    if @data[:left_twist]
      p = NMatrix.float(4,4).unit
      #if y direction of tracker is WRONG we need to rotate around z axis by 180 deg
      p[0,0], p[0,1], p[1,0], p[1,1] = Math.cos(Math::PI),-Math.sin(Math::PI),Math.sin(Math::PI),Math.cos(Math::PI)
      ilog "Will multiply left hand with this polarity matrix: #{p.inspect}"
      @data[:left_polarity] = p
    end
    #and the right hand
    @data[:right_polarity] = NMatrix.float(4,4).unit
    if @data[:right_twist]
      p = NMatrix.float(4,4).unit
      #if y direction of tracker is WRONG we need to rotate around z axis by 180 deg
      p[0,0], p[0,1], p[1,0], p[1,1] = Math.cos(Math::PI),-Math.sin(Math::PI),Math.sin(Math::PI),Math.cos(Math::PI)
      ilog "Will multiply right hand with this polarity matrix: #{p.inspect}"
      @data[:right_polarity] = p
    end
  end  
end

#for testing
if false
  tdp = TrackerDataParser.new "V06" #will select table TrackerTest
  require "openglviewer"
  ogv = OpenGlViewer.new tdp.data
  ogv.run
end
