=begin
  This class is a little B1 specific. It implements the graph we'll later
  visualize.
=end

require "rubygems"
require "narray"
require "Qt4"
require "rofl"
require "b1trackerdata/b1_data_parser"

class Graph
  
  attr_accessor :points,:deltas
  
  def initialize trial,deltas,hfrac,test=false
    #deltas we will compute
    @deltas = deltas
    ilog "Will compute deltas for #{@deltas.inspect}."
    #in case the deltas are to small/big change this factor
    @delta_factor = 256
    #in case the angles are to small/big change this factor
    @euler_factor = hfrac
    #this factor governs how many pixels the points are away from another
    @x_factor = 4
    #get the data from the database
    parser = TrackerDataParser.new trial,test
    @data = parser.data
    #generate unique array with all occuring timestamps and prepare @values
    @timestamps = build_timestamps
    #fill values (deltas,etc.)
    @previous = {:left => NMatrix.float(4,4), :right => NMatrix.float(4,4)}
    fill_values
    #check/filter broken entires
    filter_broken_entries @data
    #build the points array (this will be used later on to draw the graph)
    @points = build_points
  end
  
  #filter broken entries
  def filter_broken_entries data
    dlog "Now checking for broken entries."
    [:left,:right].each do |hand|
      dlog data[hand].first.inspect
      @dot = {:alpha => [],:beta => [], :gamma => [], :values => []}
      data[hand].sort.each do |ts,values|
        a = values[:euler][:alpha][:value]
        b = values[:euler][:beta][:value]
        g = values[:euler][:gamma][:value]
        delta_over_time a,b,g
        #dlog "alpha dot for ts: '#{ts}' is '#{@dot[:alpha_dot]}'"
        @dot[:values] << values
        if @dot[:alpha_dot] > 1 or @dot[:beta_dot] > 1 or @dot[:gamma_dot] > 1
          @dot[:values].each { |v| v[:blackout] = true }
        end
      end
    end
  end
  
  #filter using delta over time
  def delta_over_time a,b,g
    gate = 25
    @dot[:values].delete_at(0) if @dot[:values].length > gate
    @dot[:alpha] << a
    @dot[:beta] << b
    @dot[:gamma] << g 
    [:alpha,:beta,:gamma].each do |abg|
      @dot[abg].delete_at(0) if @dot[abg].length > gate
      delta = 0
      (0..@dot[abg].length-2).each do |i|
        c = @dot[abg][i]
        n = @dot[abg][i+1]
        delta += (c-n).abs
      end
      @dot["#{abg}_dot".to_sym] = delta
    end
  end
  
  #build an array with all occuring timestamps
  def build_timestamps
    @values = {}
    tss = []
    @data[:reference].each { |t,v| tss << t }
    @data[:left].each { |t,v| tss << t }
    @data[:right].each { |t,v| tss << t }
    dlog "tss count: #{tss.length}"
    timestamps = (tss.uniq).sort
    dlog "Unique timestamps: #{timestamps.length}"
    dlog "Unique timestamps first: #{timestamps.first}"
    #now preparing @values
    timestamps.each do |t|
      @values[t] = {:left => {}, :right => {}}
    end
    return timestamps
  end  
  
  #fill values with parsed data
  def fill_values
    [:left,:right].each do |hand|
      dlog "Data for #{hand.inspect} has #{@data[hand].length} entries."
      #it's crucial to do sort here, otherwise the timestamps are mixed up
      @data[hand].sort.each do |t,v|
        wlog "Empty or nil value hash!" if v.nil? or v.empty?
        #we have tracker data
        v[:blackout] = false
        #will only happen the first time
        @previous[hand] = v[:relative] if @previous[hand].abs.sum == 0.0
        #compute euler angles
        v[:euler] = compute_euler_angles(v)
        #compute deltas for plotting
        v[:deltas] = compute_deltas(hand,v)
        #for next delta computation
        @previous[hand] = v[:relative]
        #put values into @values
        @values[t][hand] = v
      end
    end
    dlog "Processed #{@values.length} values."
  end
  
  #compute euler angles for plotting
  #http://en.wikipedia.org/wiki/Euler_angles
  def compute_euler_angles values
    #we'll put the angles in here
    e = {}
    #get the hand matrix
    m = values[:relative]
    #alpha = atan2(-Z2, Z1)
    e[:alpha] = { :value => Math.atan2(-1.0*m[2,1],m[2,0]) }
    #beta = atan2(Z3, sqrt(Z1^2+Z2^2))
    e[:beta] = { :value => Math.atan2(m[2,2],Math.sqrt(m[2,0]**2+m[2,1]**2)) }
    #gamma = -atan2(Y3,-X3) if Z3 < 0
    e[:gamma] = { :value => -1.0*Math.atan2(m[1,2],-1.0*m[0,2]) } if m[2,2] <= 0.0
    #gamma = atan2(Y3,X3) if Z3 > 0
    e[:gamma] = { :value => Math.atan2(m[1,2],m[0,2]) } if m[2,2] > 0.0
    return e
  end  
  
  #compute deltas for plotting
  def compute_deltas hand,values,t=0
    #we'll put the deltas in here
    ds = {}
    #get the matrix
    m = values[:relative]
    #to reduce computation time we only compute what's needed
    if @deltas.include? :simple
      #compute simple delta over the complete 4x4 matrix
      value = (m-@previous[hand]).abs.sum
      ds[:simple] = { :value => value }
    end
    if @deltas.include? :pos
      #compute simple delta over the position vector
      value = (m[3,0..2]-@previous[hand][3,0..2]).abs.sum
      ds[:pos] = { :value => value }
    end
    if @deltas.include? :rot
      #compute simple delta over the 3x3 rotation matrix
      value = (m[0..2,0..2]-@previous[hand][0..2,0..2]).abs.sum
      ds[:rot] = { :value => value }  
    end
    if @deltas.include? :xrot
      #compute simple delta over the x-rotation axis vector
      value = (m[0,0..2]-@previous[hand][0,0..2]).abs.sum
      ds[:xrot] = { :value => value }  
    end
    if @deltas.include? :yrot
      #compute simple delta over the y-rotation axis vector
      value = (m[1,0..2]-@previous[hand][1,0..2]).abs.sum
      ds[:yrot] = { :value => value }
    end
    if @deltas.include? :zrot
      #compute simple delta over the z-rotation axis vector
      value = (m[2,0..2]-@previous[hand][2,0..2]).abs.sum
      ds[:zrot] = { :value => value } 
    end     
    #and return the computed deltas
    return ds
  end
  
  #build the points array we'll use later on to draw the graph
  def build_points
    @timestamps.each_index do |i|
      ts = @timestamps[i]
      #do the following for both hands
      [:left,:right].each do |h|
        #if tracker data are missing we substitute
        if @values.has_key? ts
          @values[ts][h] = get_missing_values if @values[ts][h].empty?
        end
        #set index and x position
        @values[ts][h][:i] = i
        x = i*@x_factor
        @values[ts][h][:x] = x
        #now we create a Qt::PointF.new(x,y) for each euler angle
        @values[ts][h][:euler].each do |name,a|
          #create Point, mirror hands on x axis and apply y-scaling
          y = 0
          unless @values[ts][h][:blackout]
            y += @euler_factor-a[:value]*@euler_factor/Math::PI 
            y *= -1.0 if h.eql? :left
          end
          #and add the point to the euler values
          @values[ts][h][:euler][name][:point] = Qt::PointF.new(x,y)
        end
        #now we create a Qt::PointF.new(x,y) for every delta type
        @values[ts][h][:deltas].each do |type,delta|
          #create Point, mirror hands on x axis and apply y-scaling
          y = delta[:value]*@delta_factor
          y *= -1.0 if h.eql? :left
          #and add the point to the delta values
          @values[ts][h][:deltas][type][:point] = Qt::PointF.new(x,y)
        end
      end  
    end
    dlog "Processed #{@values.length} timestamps."   
    points = @values.sort
    dlog "points array has #{points.length} entries."
    return points
  end

  #in case values are missing for timestamps (no tracker data), we'll supply values
  def get_missing_values
    #we'll put the deltas in here
    missing_deltas = {}
    #put 0.0 to deltas
    @deltas.each { |d| missing_deltas[d] = { :value => 0.0 } }
    #we'll put the euler angles in here
    missing_euler = {}
    #put 0.0 to euler angles
    [:alpha,:beta,:gamma].each { |a| missing_euler[a] = { :value => 0.0 } }
    #and build the values array
    v = { :deltas => missing_deltas, :euler => missing_euler, :blackout => true }
    return v
  end
end
