=begin
  This class is used to generate test Tracker-Data to validate and debug
  the other software.
=end

require "rubygems"
require "narray"
require "sequel"
require "rofl"

#connect to the database
DB = Sequel.connect(:adapter=>'mysql', :host=>'localhost', :database=>'chunky_real', :user=>'root', :password=>'mysql!secret')

class TrackerTestDataGenerator
  
  def initialize
    tablename = "TrackerTest"
    #drop old test tables
    #DB.drop_table tablename.to_sym
    #create new table for test data
    create_test_table tablename
    #our access to the db is here
    @rows = DB[tablename.to_sym]
    @freq = 50.0 #data frequency in Hz
    length = 60.0 #in seconds
    generate_test_data length
    ilog "Generated #{@rows.count} test frames."
  end
  
  #this method will generate some easy to debug test tracker data for Neck,Left Hand,Right Hand
  def generate_test_data length
    chunk = 1.0 / @freq #frame distance in seconds
    frames = length * @freq #number of frames per identifier
    #now we generate the test frames
    (0..frames).each do |i|
      timestamp = i*chunk
      m = NMatrix.float(4,4).unit
      #identifier specific changes
      write_matrix_to_db timestamp,"Neck",m
      write_matrix_to_db timestamp,"Left_Hand",get_left_hand(i)
      write_matrix_to_db timestamp,"Right_Hand",get_right_hand(i)
    end
  end

  #generate test data for left hand
  def get_left_hand index  
    m = NMatrix.float(4,4).unit
    #Left hand changes to position vector
    m[3,0] = -1 #x
    m[3,1] = 0.2 #y
    m[3,2] = -0.5 #z
    #make potential BOH point to BAB
    rot_x = x_rotation_matrix -0.5 #rotate 90 degrees around x axis
    m *= rot_x
    #and now rotate around BOH axis at 1Hz
    rot_y = y_rotation_matrix(-index*(2.0/@freq))
    m *= rot_y
    return m
  end
  
  #generate test data for right hand
  def get_right_hand index  
    m = NMatrix.float(4,4).unit
    #Right hand changes to position vector
    m[3,0] = 1 #x
    m[3,1] = 0.2 #y
    m[3,2] = -0.5 #z
    #make potential BOH point to BAB
    rot_x = x_rotation_matrix -0.5 #rotate 90 degrees around x axis
    m *= rot_x
    #and now rotate around BOH axis at 1Hz
    rot_y = y_rotation_matrix(index*(2.0/@freq))
    m *= rot_y
    return m
  end
  
  #get a rotation matrix for a rotation around the x axis
  def x_rotation_matrix radian
    m = NMatrix.float(4,4).unit
    m[1,1] = Math.cos(radian*Math::PI)
    m[1,2] = Math.sin(radian*Math::PI)
    m[2,1] = Math.sin(radian*Math::PI)*(-1.0)
    m[2,2] = Math.cos(radian*Math::PI)
    return m
  end
  
  #get a rotation matrix for a rotation around the y axis
  def y_rotation_matrix radian
    m = NMatrix.float(4,4).unit
    m[0,0] = Math.cos(radian*Math::PI)
    m[0,2] = Math.sin(radian*Math::PI)*(-1.0)
    m[2,0] = Math.sin(radian*Math::PI)
    m[2,2] = Math.cos(radian*Math::PI)
    return m
  end
  
  #get a rotation matrix for a rotation around the z axis
  def z_rotation_matrix radian
    m = NMatrix.float(4,4).unit
    m[0,0] = Math.cos(radian*Math::PI)
    m[0,1] = Math.sin(radian*Math::PI)
    m[1,0] = Math.sin(radian*Math::PI)*(-1.0)
    m[1,1] = Math.cos(radian*Math::PI)
    return m
  end
  
  #write matrix to db    
  def write_matrix_to_db timestamp,identifier,matrix   
    m = matrix.transpose #matrix data in the db are transposed
    #insert the matrix into the db
    @rows.insert(:timestamp => timestamp, :identifier => identifier,
                 :xpos => m[0,3]*1000,:ypos => m[1,3]*1000,:zpos => m[2,3]*1000,             
                 :m00 => m[0,0],:m10 => m[1,0],:m20 => m[2,0],:m30 => m[3,0],
                 :m01 => m[0,1],:m11 => m[1,1],:m21 => m[2,1],:m31 => m[3,1],
                 :m02 => m[0,2],:m12 => m[1,2],:m22 => m[2,2],:m32 => m[3,2],
                 :m03 => m[0,3],:m13 => m[1,3],:m23 => m[2,3],:m33 => m[3,3])
  end

  #create table to store test data into
  def create_test_table tablename
    DB.create_table tablename.to_sym do
      primary_key :index
      String      :identifier
      Double      :timestamp
      Float       :xpos
      Float       :ypos
      Float       :zpos
      #unity matrix is default
      Float       :m00, :default => 1.0
      Float       :m01, :default => 0.0
      Float       :m02, :default => 0.0
      Float       :m03, :default => 0.0
      Float       :m10, :default => 0.0
      Float       :m11, :default => 1.0
      Float       :m12, :default => 0.0
      Float       :m13, :default => 0.0
      Float       :m20, :default => 0.0
      Float       :m21, :default => 0.0
      Float       :m22, :default => 1.0
      Float       :m23, :default => 0.0
      Float       :m30, :default => 0.0
      Float       :m31, :default => 0.0
      Float       :m32, :default => 0.0
      Float       :m33, :default => 1.0
    end
  end
end

#generate some test data
test = TrackerTestDataGenerator.new
