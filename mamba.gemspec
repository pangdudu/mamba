Gem::Specification.new do |s|
  s.name = %q{mamba}
  s.version = "0.1"

  s.specification_version = 2 if s.respond_to? :specification_version=

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-08-27}
  s.description = %q{qt-ruby tool for ART tracker data time series analysis.}
  s.email = %q{pangdudu@github}
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "lib/mamba.rb", "lib/graph.rb", 
             "lib/b1trackerdata/b1_data_parser.rb","lib/b1trackerdata/openglviewer.rb",
             "tests/generate_testdata.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/mamba}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{a ruby-qt ART tracker data analysis tool}
  s.add_dependency(%q<pangdudu-rofl>, [">= 0"])
  s.add_dependency(%q<sequel>, [">= 0"])
  s.add_dependency(%q<sequel_core>, [">= 0"])
  s.add_dependency(%q<sequel_model>, [">= 0"])
  s.add_dependency(%q<mysql>, [">= 0"])
  s.add_dependency(%q<narray>, [">= 0"])
  s.add_dependency(%q<ruby-opengl>, [">= 0"])
end
