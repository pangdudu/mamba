= Mamba

Hi girls and boys!

This is a Qt4 ruby tool for analysis of ART tracker data time series generated
by the ART motion capturing system.

== Installation

  sudo gem install pangdudu-mamba --source=http://gems.github.com
	 	 
== Usage

  ruby mamba.rb
  
== Config

Is still hardcoded in the source files. :)

== Rule the universe! 

Oh mighty gods of ruby, please make the GIL go away!

== License

GPL -> http://www.gnu.org/licenses/gpl.txt
